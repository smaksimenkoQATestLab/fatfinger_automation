package properties;

import java.nio.file.Paths;

public class Properties {
    public static boolean isAndroid = getPlatform().equalsIgnoreCase("Android");

    public static String getPlatform() {
        return System.getProperty(PropertiesNames.APPIUM_PLATFORM.toString());
    }

    public static String getAppiumDeviceName() {
        return System.getProperty(PropertiesNames.APPIUM_DEVICE_NAME.toString());
    }

    public static String getAppiumUdid() {
        return System.getProperty(PropertiesNames.APPIUM_UDID.toString());
    }

    public static String getAppiumApiVersion() {
        return System.getProperty(PropertiesNames.APPIUM_API_VERSION.toString());
    }

    public static String getAppiumAppDir() {
        return System.getProperty(PropertiesNames.APPIUM_APP_DIR.toString());
    }

    public static String getConfigDir() {
        return System.getProperty(PropertiesNames.CONFIG_DIR.toString());
    }

    public static String getAppiumEmulatorTimeout() {
        return System.getProperty(PropertiesNames.APPIUM_EMULATOR_TIMEOUT.toString());
    }
    public static String getRunConfig() {
        String configFileToUse = "config.cfg";
        String directory = System.getProperty(PropertiesNames.RUN_CONFIG_DIRECTORY.toString());
        return directory == null || directory.isEmpty() ?
                Paths.get(getConfigDir(), configFileToUse).toString()
                : Paths.get(directory, configFileToUse).toString();
    }

    public static String getDevicePassword() {
        return System.getProperty(PropertiesNames.APPIUM_DEVICE_PASSWORD.toString());
    }

    public static String getEmulator() {
        return System.getProperty(PropertiesNames.APPIUM_EMULATOR.toString());
    }

    public static String getNpmPackagesFolderPath() {
        return System.getProperty(PropertiesNames.NPM_PACKAGES_FOLDER_PATH.toString());
    }

}
