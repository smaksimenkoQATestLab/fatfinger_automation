package properties;

public enum PropertiesNames {
    CONFIG_DIR("config.dir"),
    APPIUM_HOST("appium.host"),
    APPIUM_PORT("appium.port"),
    APPIUM_DEVICE_NAME("appium.deviceName"),
    APPIUM_UDID("appium.udid"),
    APPIUM_API_VERSION("appium.apiVersion"),
    APPIUM_PLATFORM("appium.platform"),
    APPIUM_EMULATOR_TIMEOUT("appium.emulatorTimeout"),
    APPIUM_APP_DIR("appium.app.dir"),
    RUN_CONFIG_DIRECTORY("run.config.directory"),
    USING_REMOTE_SERVER("usingRemoteServer"),
    APPIUM_DEVICE_PASSWORD("appium.devicePassword"),
    APPIUM_EMULATOR("appium.emulator"),
    NPM_PACKAGES_FOLDER_PATH("npmPackagesFolderPath"),
    RUN_TYPE("run.type");


    private final String propertyName;

    PropertiesNames(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getValue() {
        return System.getProperty(propertyName);
    }

    @Override
    public String toString() {
        return propertyName;
    }
}
