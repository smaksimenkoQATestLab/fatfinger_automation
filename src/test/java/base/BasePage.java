package base;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.NoSuchContextException;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Constants;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Set;

public class BasePage {
    protected WebDriverWait wait;

    protected BasePage() {
        wait = new WebDriverWait(driver(), 30000);
    }

    public static MobileDriver driver() {
        return BaseTest.driver();
    }

    protected void click(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element)).click();
    }

    protected void setCheckBox(WebElement element, boolean flag) {
        element = wait.until(ExpectedConditions.visibilityOf(element));
        if (element.isSelected() ^ flag)
            element.click();
    }

    protected boolean isElementDisplayed(int timeout, WebElement element) {
        try {
            new WebDriverWait(driver(), timeout).until(ExpectedConditions.visibilityOf(element));
            return true;
        } catch (Exception te) {
            System.out.println(te.getMessage());
            return false;
        }
    }

    protected boolean isElementExists(int timeout, By locator) {
        try {
            new WebDriverWait(driver(), timeout).until(ExpectedConditions.presenceOfElementLocated(locator));
            return true;
        } catch (Exception te) {
            return false;
        }
    }

    protected void clickByJS(String locator) {
        isElementExists(Constants.ELEMENT_30_TIMEOUT_SECONDS, By.xpath(locator));
        ((JavascriptExecutor) driver()).executeScript("document.evaluate(arguments[0]" +
                ", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();", locator);
    }

    protected void clickByJS(WebElement element) {
        sleep(1000);
        ((JavascriptExecutor) driver()).executeScript("arguments[0].click();", element);
    }

    @Step
    public void setContextToWebview() {
        Set<String> contextNames = driver().getContextHandles();
        System.out.println("Available contexts :");
        for (String contextName : contextNames) {
            System.out.println(contextName);
        }
        try {
            Set<String> availableContexts = driver().getContextHandles();
            availableContexts.stream()
                    .filter(context -> context.toLowerCase().contains("seeforge"))
                    .forEach(newContext -> driver().context(newContext));
            System.out.println("context is " + driver().getContext());
        } catch (NoSuchContextException ex) {
            Set<String> availableContexts = driver().getContextHandles();
            availableContexts.stream()
                    .filter(context -> context.toLowerCase().contains("seeforge"))
                    .forEach(newContext -> driver().context(newContext));
            System.out.println("context is " + driver().getContext());
        }
        sleep(2000);
    }

    public void setContextToWebview(String newContextName) {
        sleep(2000);
        Set<String> contextNames = driver().getContextHandles();
        System.out.println("Available contexts :");
        for (String contextName : contextNames) {
            System.out.println(contextName); //prints out something like NATIVE_APP \n WEBVIEW_1
        }

        try {
            Set<String> availableContexts = driver().getContextHandles();
            availableContexts.stream()
                    .filter(context -> context.toLowerCase().contains(newContextName))
                    .forEach(newContext -> driver().context(newContext));
            System.out.println("context is " + driver().getContext());
        } catch (NoSuchContextException ex) {
            Set<String> availableContexts = driver().getContextHandles();
            availableContexts.stream()
                    .filter(context -> context.toLowerCase().contains(newContextName))
                    .forEach(newContext -> driver().context(newContext));
            System.out.println("context is " + driver().getContext());
        }
        sleep(4000);
    }

    @Step
    public void setContextToNative() {
        driver().context("NATIVE_APP");
        System.out.println("context is " + driver().getContext());
        sleep(2000);
    }

    protected void sleep(int miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected BufferedImage getScreenshot() {
        File screen = ((TakesScreenshot) driver()).getScreenshotAs(OutputType.FILE);
        BufferedImage img = null;
        try {
            img = ImageIO.read(screen);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (img != null)
                img.flush();
        }
        return img;
    }

    protected BufferedImage getElementScreenshot(WebElement element) throws IOException {
        File screen = ((TakesScreenshot) driver()).getScreenshotAs(OutputType.FILE);
        BufferedImage img = ImageIO.read(screen);
        int widthElement = element.getSize().getWidth();
        int heightElement = element.getSize().getHeight();
        float koeffX = (float) img.getWidth() / (float) getWebViewResolution().getWidth();
        float koeffY = (float) img.getHeight() / (float) getWebViewResolution().getHeight();
        BufferedImage dest = img.getSubimage(Math.round(koeffX * element.getLocation().getX()), Math.round(koeffY * element.getLocation().getY())
                , Math.round(koeffX * widthElement), Math.round(koeffY * heightElement));
        return dest;
    }

    protected Dimension getWebViewResolution() {
        JavascriptExecutor js = (JavascriptExecutor) driver();
        int width = ((Long) js.executeScript("return window.innerWidth || document.body.clientWidth")).intValue();
        int height = ((Long) js.executeScript("return window.innerHeight || document.body.clientHeight")).intValue();
        return new Dimension(width, height);

    }

    protected Dimension getScreenResolution() {
        int width = Integer.parseInt(((String) ((AndroidDriver) driver()).getCapabilities().getCapability("deviceScreenSize")).split("x")[0]);
        int height = Integer.parseInt(((String) ((AndroidDriver) driver()).getCapabilities().getCapability("deviceScreenSize")).split("x")[1]);
        return new Dimension(width, height);
    }


    protected void waitForWebViewLoad() {
        wait.until((ExpectedCondition<Boolean>) wdriver ->
                ((String) ((JavascriptExecutor) driver()).executeScript("return document.readyState")).equals("complete"));
    }

    protected void swipe() {
        TouchAction myAction = new TouchAction(driver());
        int height = getScreenResolution().getHeight();
        int width = getScreenResolution().getWidth();
        myAction.press(PointOption.point(width / 2, height / 2))
                .moveTo(PointOption.point(width / 2, height / 2 + height / 100 * 10))
                .release().perform();
    }

    public void waitForWiFiConnected() {
        sleep(2000);
        wait.until((ExpectedCondition<Boolean>) wdriver -> ((AndroidDriver) driver()).getConnection().isWiFiEnabled());
    }

    public boolean compareImages(BufferedImage imgA, BufferedImage imgB) {
        // The images must be the same size.
        if (imgA.getWidth() != imgB.getWidth() || imgA.getHeight() != imgB.getHeight()) {
            return false;
        }
        int width = imgA.getWidth();
        int height = imgA.getHeight();
        // Loop over every pixel.
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // Compare the pixels for equality.
                if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
                    return false;
                }
            }
        }
        return true;
    }
}
