package base;

import annotations.Combine;
import annotations.WebTest;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import utils.Constants;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.open;


public class BaseTest {
    public static final String USERNAME = "sergeymaksimenko3";
    public static final String AUTOMATE_KEY = "ra4dmfgvEY63V29VJ2pL";

    public static final String url = "http://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
    public static MobileDriver driver;
    public RemoteWebDriver remoteDriver;
    private static AppiumDriverLocalService service;
    private String deviceName = "";
    private String platformVersion = "";
    private String buildName = System.getProperty("build.name");

    public static MobileDriver driver() {
        return driver;
    }

    @BeforeClass
    public void setUp() {
       /* service = AppiumDriverLocalService.buildDefaultService();
        service.start();

        if (service == null || !service.isRunning()) {
            throw new AppiumServerHasNotBeenStartedLocallyException(
                    "An appium server node is not started!");
        }*/
    }

    @Step
    public void setUpMobileForBrowserStack(String testName) throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("build", buildName);
        capabilities.setCapability("name", testName);
        capabilities.setCapability("browserstack.debug", true);
        capabilities.setCapability("device", deviceName);
        capabilities.setCapability("os_version", platformVersion);
        capabilities.setCapability("autoGrantPermissions", true);
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("nativeWebScreenshot", true);
        capabilities.setCapability("browserName","Chrome");
        capabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true);
        capabilities.setCapability("app", "bs://2033b12add64d0124cb79058b2f4c86e4e6352a8");

        driver = new AndroidDriver(new URL(url), capabilities);

    }

    @BeforeMethod
    public void assureSessionExist(Method testMethod) throws MalformedURLException {
        if (System.getProperty("browserstack").equals("true")) {
            if (System.getProperty("bsdevice").equals(""))
                deviceName = "Samsung Galaxy S7";
            else deviceName = System.getProperty("bsdevice");

            if (System.getProperty("platform.version").equals(""))
                platformVersion = "7.0";
            else platformVersion = System.getProperty("platform.version");

            if (testMethod.isAnnotationPresent(WebTest.class))
                setUpWebDriver();
            else {
                if (testMethod.isAnnotationPresent(Combine.class)) {
                    setUpWebDriver();
                    setUpMobileForBrowserStack(testMethod.getAnnotation(Description.class).value());
                } else {
                    setUpMobileForBrowserStack(testMethod.getAnnotation(Description.class).value());
                }
            }
        } else {
            if (testMethod.isAnnotationPresent(WebTest.class))
                setUpWebDriver();
            else if (testMethod.isAnnotationPresent(Combine.class)) {
                setUpWebDriver();
                setUpForAndroidRun();
                executableAdbConnect();
            } else {
                setUpForAndroidRun();
                executableAdbConnect();
            }
        }
    }

    @AfterClass()
    public void tearDown() {
        if (driver() != null) {
            driver.quit();
        }
    }

    public void executableAdbConnect() {
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        System.out.println("Executing adb connect . . .");

        try {
            String[] adb_cmd = {Constants.adbPath, "connect"};
            int pstatus = new ProcessBuilder(adb_cmd).start().waitFor();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setUpWebDriver() {
        Configuration.timeout = 20000;
        Configuration.collectionsTimeout = 20000;
        open(System.getProperty("baseUrl"));
    }

    public void setUpWebDriverForBrowserStack() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "62.0");
        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("resolution", "1024x768");
        remoteDriver = new RemoteWebDriver(new URL(url), caps);
        remoteDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverRunner.setWebDriver(remoteDriver);
    }

    @Step
    public void setUpForAndroidRun() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "MEIZU M5");
        capabilities.setCapability("platformVersion", "7.0");
        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("autoGrantPermissions", true);
        capabilities.setCapability("nativeWebScreenshot", true);
        //capabilities.setCapability("fullReset", "true");
        capabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true);
        capabilities.setCapability("app", System.getProperty("user.dir") + "\\config\\android-debug-1.0.10.apk");
        capabilities.setCapability("newCommandTimeout", 300);

        URL address = new URL("http://localhost:4723/wd/hub");
        driver = new AndroidDriver<>(address, capabilities);

    }

    @Step
    protected void toggleWiFi(boolean flag) {
        Runtime rt = Runtime.getRuntime();
        String command = String.format("adb shell am broadcast -a io.appium.settings.wifi --es setstatus %s",
                flag ? "enable" : "disable");
        try {
            rt.exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step
    protected void clickBack() {
        ((AndroidDriver) driver()).pressKeyCode(AndroidKeyCode.BACK);
    }

    @Step
    protected void setScreenorientation(ScreenOrientation orientation) {
        driver().rotate(orientation);
    }

    @Step
    public ImageDiff getDiff(BufferedImage start, BufferedImage end) {
        ImageDiff diff = new ImageDiffer().makeDiff(start, end);
        return diff.withDiffSizeTrigger(5000);
    }

}
