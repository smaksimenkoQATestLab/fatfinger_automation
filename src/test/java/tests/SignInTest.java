package tests;

import actions.LoginAction;
import annotations.Combine;
import annotations.WebTest;
import base.BaseTest;
import io.qameta.allure.Description;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.ScreenOrientation;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.*;
import pages.appNew.AppApprovePage;
import pages.appNew.ChooseAppPage;
import pages.appNew.SignaturePage;
import pages.popup.*;
import pages.web.AppsLibraryPage;
import pages.web.CreateAppPage;
import pages.web.MenuPage;
import pages.web.ReportNewActivityPage;
import utils.Constants;

import java.awt.image.BufferedImage;
import java.io.IOException;

import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.page;

public class SignInTest extends BaseTest {
    String appName;

    @Test
    @Description("Login with existing account ")
    public void tc606212() {
        LoginPage loginPage = new LoginPage();
        loginPage.isPageOpened();
        loginPage.enterEmail(System.getProperty("username"))
                .enterPass(System.getProperty("userpass"))
                .clickLogin();
        WelcomeSlidesPage welcomePage = new WelcomeSlidesPage();
        Assert.assertTrue(welcomePage.isWelcomeScreenVisible(), "Welcome screen doesn't appear");
    }

    @Test
    @Description("Log in with invalid credentials")
    public void tc606213() {
        LoginPage loginPage = new LoginPage();
        loginPage.isPageOpened();
        loginPage.enterEmail(System.getProperty("username") + "wrong")
                .enterPass(System.getProperty("userpass") + "wrong")
                .clickLogin();
        AccessDeniedPopup accessDenied = new AccessDeniedPopup();
        Assert.assertTrue(accessDenied.isPopupVisible(), "Access denied popup doesn't appear");
    }

    @Test
    @Description("Make sure the user can log out the application")
    public void tc606750() {
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        HomeMenuPage homeMenu = home.clickMenu();
        LogoutConfirmPopup logOut = homeMenu.clickLogOut();
        LoginPage login = logOut.clickYes();
        Assert.assertTrue(login.isPageOpened(), "Login page doesn't appear");
    }

    @Test
    @Description("Make sure that forgot your password letter is sending")
    public void tc606747() {
        LoginPage loginPage = new LoginPage();
        loginPage.clickForgotPasswordLink();
        ForgotPasswordPage forgotPasswordPage = new ForgotPasswordPage();
        forgotPasswordPage
                .enterEmail(System.getProperty("username"))
                .clickEmailLinkButton();
        Assert.assertTrue(forgotPasswordPage.isResetHeaderTextDisplayed(), "Header text is not displayed");
        //TODO - create email check
    }

    @Test
    @Description("Make sure that user can launch the 'Register in app' link")
    public void tc606760() {
        LoginPage loginPage = new LoginPage();
        loginPage
                .clickSignUPLink()
                .waitForPageLoad();
    }

    @WebTest
    @Description("Make sure the apps are displayed in the 'New' tab - web")
    @Test
    public void tc606742_web() {

        appName = "testApp_" + RandomStringUtils.randomAlphabetic(5);
        String sampleItem = "sampleItem_" + RandomStringUtils.randomAlphabetic(5);
        pages.web.LoginPage loginPage = page(pages.web.LoginPage.class);
        AppsLibraryPage appsPage = loginPage.enterEmail(System.getProperty("username"))
                .enterPass(System.getProperty("userpass"))
                .clickLogin();
        CreateAppPage createAppPage = appsPage.waitForPageLoad().clickBurgerMenu().clickCreateNewApp();
        AppsLibraryPage appsLibrary = createAppPage.enterAppName(appName).enterSingleTextField(0, sampleItem).clickSaveGoBack();
        appsLibrary.switchStatus(appName).clickYes().checkStatus(appName, AppsLibraryPage.Status.PUBLISHED);
        System.out.println(appName);
        close();
    }

    @Test(dependsOnMethods = {"tc606742_web"})
    @Description("Make sure the apps are displayed in the 'New' tab - mobile")
    public void tc606742_mob() {
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home.clickNewButton()
                .waitForPageLoad();
        Assert.assertTrue(chooseAppPage.isAppPresent(appName), "App " + appName + " is not present");
    }

    @Test
    @Description("Make sure the app can be filled and approve")
    public void tc606223() {
        String appName = "testAppName1";
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        SubmitActivityPopup submitActivity = chooseAppPage
                .clickAppByName(appName)
                .waitForPageLoad()
                .enterTitle("testTitle" + RandomStringUtils.randomAlphabetic(5))
                .enterSampleItem("testItem")
                .clickCheckmark();
        AppApprovePage page = submitActivity.clickNotYet();
        page.waitForPageLoad();
        submitActivity = page.clickCheckmark();
        home = submitActivity.clickYes();
        home.waitForPageLoad();
    }

    @Test
    @Description("Make sure the app can not be submitted without the title")
    public void tc606224() {
        String appName = "testAppName1";
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        chooseAppPage
                .clickAppByName(appName)
                .waitForPageLoad()
                .enterSampleItem("testItem")
                .clickCheckmark();
        EnterTitlePopup popup = new EnterTitlePopup();
        popup.isPopupVisible();
        popup.clickOk();
        AppApprovePage page = new AppApprovePage();
        page.waitForPageLoad();
    }

    @Test
    @Description("Make sure it is possible close filled app without saving")
    public void tc606665() {
        String appName = "testAppName1";
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        DiscardChangesPopup discardChanges = chooseAppPage
                .clickAppByName(appName)
                .waitForPageLoad()
                .enterTitle("testTitle" + RandomStringUtils.randomAlphabetic(5))
                .enterSampleItem("testItem")
                .clickClose();
        discardChanges.waitForPopUpLoad().clickNotYet();
        AppApprovePage appApprovePage = new AppApprovePage();
        discardChanges = appApprovePage.clickClose();
        discardChanges.waitForPopUpLoad().clickYes();
        home.waitForPageLoad();
        //TODO need to check app is not submitted
    }

    @Test
    @Description("Make sure it is possible to skip Welcome slides")
    public void tc606227() {
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        HomeMenuPage homeMenu = home.clickMenu();
        WelcomeSlidesPage slides = homeMenu.clickSlides();
        slides.waitForPopUpLoad().clickSkip();
        home.waitForPageLoad();
    }

    @Test
    @Description("Make sure it is possible to see the complete Welcome slides presentation")
    public void tc606228() {
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        HomeMenuPage homeMenu = home.clickMenu();
        WelcomeSlidesPage slides = homeMenu.clickSlides().waitForPopUpLoad();
        for (int i = 0; i < 5; i++) {
            slides.goToSlide(i);
        }
        home = slides.clickContinue();
        home.waitForPageLoad();
    }

    @Test
    @Description("Make sure the Welcome slides are displayed default after 1st launch on the device")
    public void tc606226() {
        new LoginAction().loginValidate();
        WelcomeSlidesPage slides = new WelcomeSlidesPage();
        for (int i = 0; i < 5; i++) {
            slides.goToSlide(i);
        }
        HomePage home = slides.clickContinue();
        home.waitForPageLoad();
    }

    @Test
    @Description("Make sure it is possible to edit activity")
    public void tc606234() {
        String activityName = "testTitleoCwER";
        String newTitle = activityName + RandomStringUtils.randomAlphabetic(5);
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseViewsPage chooseViewsPage = home
                .waitForPageLoad()
                .clickViewButton()
                .waitForPageLoad();
        AppApprovePage appApprovePage = chooseViewsPage.clickViewByIndex(0).waitForPageLoad();
        appApprovePage.clickMenu().waitForPageLoad().clickEditActivity().waitForPageLoad();
        home = appApprovePage
                .enterTitle(newTitle)
                .clickCheckmark()
                .clickYes();
        home.waitForPageLoad().clickViewButton().waitForPageLoad();
        Assert.assertTrue(chooseViewsPage.isViewPresent(newTitle), "Activity with new title should be exists in list");
        //TODO check name title has been changed
    }

    @Test
    @Description("Make sure the 'Digital Signature' app element can be filled")
    public void tc606229() throws IOException {
        String appName = "testAppName1";
        String signName = "signName";
        String activityTitle = "testTitle" + RandomStringUtils.randomAlphabetic(5);
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        SignaturePage signaturePage = chooseAppPage
                .clickAppByName(appName)
                .waitForPageLoad()
                .enterTitle(activityTitle).clickSignature();
        SubmitActivityPopup submitActivityPopup = signaturePage.waitForPageLoad().enterSignatoryName(signName)
                .makeSignatore()
                .clickCheck();
        submitActivityPopup.clickYes();
        AppApprovePage appApprovePage = new AppApprovePage();
        BufferedImage signStart = appApprovePage.getSignScreenshot();
        appApprovePage.waitForPageLoad().clickCheckmark()
                .clickYes().waitForPageLoad();
        appApprovePage = home.waitForPageLoad()
                .clickViewButton()
                .waitForPageLoad()
                .clickViewByName(activityTitle)
                .waitForPageLoad();
        BufferedImage signFinish = appApprovePage.getSignScreenshot();
        Assert.assertFalse(getDiff(signStart, signFinish).hasDiff(), "Signatures are not the same");
        Assert.assertEquals(signName, appApprovePage.getSignatureName(), "Name signature is not the same");
    }

    @Test
    @Description("Make sure it is possible to search an activity from the list")
    public void tc606741() {
        String activityName = "testTitleoCwER";
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseViewsPage chooseViewsPage = home
                .waitForPageLoad()
                .clickViewButton()
                .waitForPageLoad();
        chooseViewsPage.clickSearch().enterSearch(activityName);
        Assert.assertTrue(chooseViewsPage.getFoundActivitiesAmount(activityName), "Search activities not found");
        chooseViewsPage.clickCloseSearch();
        Assert.assertTrue(chooseViewsPage.isSearchFieldEmpty(), "Search field should be empty");
        String wrongNameActivity = activityName + RandomStringUtils.randomAlphabetic(5);
        chooseViewsPage.enterSearch(wrongNameActivity);
        Assert.assertFalse(chooseViewsPage.getFoundActivitiesAmount(wrongNameActivity), "List of found activities should be empty");
        chooseViewsPage.clickArrowForward().waitForPageLoad();
        Assert.assertTrue(chooseViewsPage.isTitleBarExists(), "Search field should be invisible");
        Assert.assertTrue(chooseViewsPage.getActivitiesAmount() > 0, "List of activities shouldn't be empty");
    }

    @Test
    @Description("Make sure the activity can be added offline")
    public void tc606761() throws IOException {
        String appName = "testAppName1";
        String title = "testTitle" + RandomStringUtils.randomAlphabetic(5);

        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        Assert.assertTrue(chooseAppPage.isListOfAppsDisplayed(), "The list of apps is not displayed.");

        AppApprovePage appApprovePage = chooseAppPage.
                clickAppByName(appName)
                .waitForPageLoad();
        Assert.assertTrue(appApprovePage.getNameOfApp().equals(appName), "The app is not opened.");
        Assert.assertTrue(appApprovePage
                .enterTitle(title)
                .waitForPageLoad()
                .getTitle()
                .equals(title), "The title is not entered");

        toggleWiFi(false);
        SubmitActivityPopup submitActivityPopup = appApprovePage.clickCheckmark();
        Assert.assertTrue(submitActivityPopup.isPopupVisible(), "The confirmation pop-up is not displayed");

        home = submitActivityPopup.clickYes().waitForPageLoad();
        Assert.assertTrue(home.isSuccessfulSubmittingMessageShown(), "The message about the successful submitting is not shown");

        ChooseViewsPage chooseViewsPage = home.clickViewButton();
        Assert.assertTrue(chooseViewsPage.isListOfActivitiesDisplayed(), "The activities are not shown");
        Assert.assertTrue(chooseViewsPage.isActivitySyncPending(title), "The added activity has not status 'Sync pending'");

        // turns on the WiFi onto device
        toggleWiFi(true);
    }

    @Description("Make sure it is possible to create sub-activity")
    @Test
    public void tc606710() {
        String activityName = "testTitleoCwER";
        String appName = "testAppName1";
        String newTitle = activityName + RandomStringUtils.randomAlphabetic(5);
        String newSampleItem = activityName + RandomStringUtils.randomAlphabetic(5);
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseViewsPage chooseViewsPage = home
                .waitForPageLoad()
                .clickViewButton()
                .waitForPageLoad();
        AppApprovePage appApprovePage = chooseViewsPage.clickViewByName(activityName).waitForPageLoad();
        ChooseAppPage appPage = appApprovePage.clickMenu().waitForPageLoad().clickCreateSubActivity().waitForPageLoad();
        home = appPage.clickAppByName(appName)
                .enterTitle(newTitle)
                .enterSampleItem(newSampleItem)
                .clickCheckmark()
                .clickYes().waitForPageLoad();
        //todo check new activity exists
    }

    @Test
    @Description("Make sure the 'Back' button works correctly")
    public void tc606740() {
        String activityName = "testTitleoCwER";
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseViewsPage chooseViewsPage = home
                .waitForPageLoad()
                .clickViewButton()
                .waitForPageLoad();
        chooseViewsPage.clickViewByName(activityName).waitForPageLoad();
        clickBack();
        chooseViewsPage.waitForPageLoad();
        Assert.assertTrue(chooseViewsPage.getActivitiesAmount() > 0, "List of activities shouldn't be empty");
        clickBack();
        home.waitForPageLoad().clickTasksButton().waitForPageLoad();
        clickBack();
        home.waitForPageLoad().clickMapButton().waitForPageLoad();
        clickBack();
        home.waitForPageLoad();
    }

    @Test
    @Description("Make sure the 'Single line text' app element can be filled")
    public void tc606752() {
        String appName = "My testing app (Single line text)";
        String title = "testTitle" + RandomStringUtils.randomAlphabetic(5);
        String sampleText = "testSample" + RandomStringUtils.randomAlphabetic(5);

        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        Assert.assertTrue(chooseAppPage.isListOfAppsDisplayed(), "The list of apps is not displayed.");
        AppApprovePage appApprovePage = chooseAppPage.
                clickAppByName(appName)
                .waitForPageLoad();
        Assert.assertTrue(appApprovePage.getNameOfApp().equals(appName), "The app is not opened.");
        appApprovePage.enterSampleItem(sampleText).waitForPageLoad();
        Assert.assertFalse(appApprovePage.getSampleItemAlreadyFilledText().isEmpty()
                , "The 'Single line text' field is not editable or the text is not entered into it");
        Assert.assertTrue(appApprovePage
                .enterTitle(title)
                .waitForPageLoad()
                .getTitle()
                .equals(title), "The title is not entered");
        SubmitActivityPopup submitActivityPopup = appApprovePage.clickCheckmark();
        Assert.assertTrue(submitActivityPopup.isPopupVisible(),
                "The confirmation pop-up is not displayed");

        home = submitActivityPopup.clickYes().waitForPageLoad();
        Assert.assertTrue(home.isSuccessfulSubmittingMessageShown(),
                "The message about the successful submitting is not shown");

        ChooseViewsPage chooseViewsPage = home.clickViewButton();
        Assert.assertTrue(chooseViewsPage.isListOfActivitiesDisplayed(),
                "The activities are not shown in the 'View activities' page");

        Assert.assertTrue(chooseAppPage
                .clickAppByTitle(title)
                .waitForPageLoad()
                .getSampleItemAlreadyFilledText()
                .equalsIgnoreCase(sampleText), "The entered text into the 'Single line text' field is not displayed correctly on the activity page");

    }

    @Combine
    @Description("Make sure that data is saved when form was changed on web during filling it in app and saving after that")
    public void tc606749() {
        String appName = "testAppName1";
        String fieldValue = RandomStringUtils.randomAlphabetic(5);
        //----mobile-------
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        //------web--------
        pages.web.LoginPage loginPage = page(pages.web.LoginPage.class);
        MenuPage menuPage = loginPage.enterEmail(System.getProperty("username"))
                .enterPass(System.getProperty("userpass"))
                .clickLogin();
        //---mobile----------------
        AppApprovePage appApprovePage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad()
                .clickAppByName(appName)
                .waitForPageLoad();
        //---web-----------
        AppsLibraryPage apps = menuPage.clickBurgerMenu().openApps();
        //----mobile-----------
        appApprovePage
                .enterTitle("testTitle" + RandomStringUtils.randomAlphabetic(5));
//---web-----------
        CreateAppPage createAppPage = apps.enterSearchField(appName).chooseAppByName(appName);
//----mobile-----------
        appApprovePage
                .enterSampleItem("testItem");
        //-----web---------
        createAppPage.makeChanges().fillAllReauiredFills().clickSaveGoBack();
        //---mobile------------
        home = appApprovePage
                .clickCheckmark()
                .clickNotYet()
                .clickCheckmark()
                .clickYes();
        home.waitForPageLoad().isToastMessageVisible();
    }

    @Test
    @Description("Make sure the app is displayed correctly in the landscape mode")
    public void tc606762() {
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        setScreenorientation(ScreenOrientation.LANDSCAPE);
        ChooseAppPage appPage = home.clickNewButton().waitForPageLoad();
        Assert.assertTrue(appPage.isListOfAppsDisplayed(), "List of apps shouldn't be empty");
        clickBack();
        home.waitForPageLoad().clickMenu().waitForPageLoad();
        clickBack();
        home.waitForPageLoad();
        ChooseViewsPage views = home.clickViewButton().waitForPageLoad();
        Assert.assertTrue(views.isListOfActivitiesDisplayed(), "List of activities shouldn't be empty");
        clickBack();
        ChooseTasksPage tasks = home.waitForPageLoad().clickTasksButton().waitForPageLoad();
        Assert.assertTrue(tasks.isTasksNotEmpty(), "List of tasks shouldn't be empty");
        clickBack();
        home.waitForPageLoad().clickMapButton().waitForPageLoad();
        clickBack();
        home.waitForPageLoad();
        setScreenorientation(ScreenOrientation.PORTRAIT);
    }

    @Test
    @Description("Make sure the 'Multiple choice 1 answer' app element can be filled")
    public void tc606754() {
        String appName = "My testing app (Multiple choice 1 answer)";
        String viewTitle = "TestTitle" + RandomStringUtils.randomAlphabetic(5);
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        AppApprovePage newAction = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad()
                .clickAppByName(appName)
                .waitForPageLoad();
        newAction.enterTitle(viewTitle).clickOneAnswer(3).clickCheckmark().clickYes().waitForPageLoad();
        home.clickViewButton().waitForPageLoad().clickViewByName(viewTitle).waitForPageLoad();
    }


    @Test
    @Combine
    @Description("Make sure that data from app is synchronized with web site")
    public void tc606757() {
        String appName = "testAppName1";
        String viewName = "testView" + RandomStringUtils.randomAlphabetic(5);
        //----mobile-------
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        //------web--------
        pages.web.LoginPage loginPage = page(pages.web.LoginPage.class);
        MenuPage menuPage = loginPage.enterEmail(System.getProperty("username"))
                .enterPass(System.getProperty("userpass"))
                .clickLogin();
        //---mobile----------------
        AppApprovePage appApprovePage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad()
                .clickAppByName(appName)
                .waitForPageLoad();
        appApprovePage.enterTitle(viewName).enterSampleItem("sample test").clickCheckmark()
                .clickYes().waitForPageLoad();
        //---web--------
        pages.web.ChooseViewsPage viewsPage = menuPage.clickView().waitForPageLoad();
        Assert.assertTrue(viewsPage.isViewPresent(viewName), "Created view should be exists on web");
        //--mobile------
        AppApprovePage view = home.clickViewButton().waitForPageLoad().clickViewByName(viewName).waitForPageLoad();
        view.clickMenu().waitForPageLoad().clickCreateTask()
                .assignTo(0).clickCheckmark().waitForToastMessage();
//---web------
        pages.web.ChooseTasksPage tasksPage = menuPage.clickTasks().waitForPageLoad();
        Assert.assertTrue(tasksPage.isTaskPresent(viewName), "Created task should be exists on web");
    }

    @Test
    @Description("Make sure the 'True/false' app element can be filled")
    public void tc606753() {
        String appName = "My testing app (True/false)";
        String title = "testTitle" + RandomStringUtils.randomAlphabetic(5);
        String sampleText = "testSample" + RandomStringUtils.randomAlphabetic(5);

        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        Assert.assertTrue(chooseAppPage.isListOfAppsDisplayed(), "The list of apps is not displayed.");

        AppApprovePage appApprovePage = chooseAppPage.
                clickAppByName(appName)
                .waitForPageLoad();
        Assert.assertTrue(appApprovePage.getNameOfApp().equals(appName), "The app is not opened.");

        appApprovePage.tapsOnYesButtonInTheTrueFalseField();
        Assert.assertTrue(appApprovePage.isYesButtonGreenAfterTapingOn(), "The button 'Yes' in the 'True/false' field is not green after clicking on it");
        appApprovePage.tapsOnNoButtonInTheTrueFalseField();
        Assert.assertTrue(appApprovePage.isNoButtonRedAfterTapingOn(), "The button 'No' in the 'True/false' field is not red after clicking on it");
        Assert.assertTrue(appApprovePage
                .enterTitle(title)
                .waitForPageLoad()
                .getTitle()
                .equals(title), "The title is not entered");

        SubmitActivityPopup submitActivityPopup = appApprovePage.clickCheckmark();
        Assert.assertTrue(submitActivityPopup.isPopupVisible(), "The confirmation pop-up is not displayed");
        home = submitActivityPopup.clickYes().waitForPageLoad();
        Assert.assertTrue(home.isSuccessfulSubmittingMessageShown(), "The message about the successful submitting is not shown");
        ChooseViewsPage chooseViewsPage = home.clickViewButton();
        Assert.assertTrue(chooseViewsPage.isListOfActivitiesDisplayed(), "The activities are not shown in the 'View activities' page");

        Assert.assertTrue(chooseAppPage
                .clickAppByTitle(title)
                .waitForPageLoad()
                .isNoButtonRedAfterTapingOn(), "The answer is not displayed correctly (No button is not red) on the activity page");
    }

    @Test
    @Description("Make sure the 'date/time' app element can be filled")
    public void tc606755() {
        String appName = "My testing app (Date/Time)";
        String title = "testTitle" + RandomStringUtils.randomAlphabetic(5);
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        Assert.assertTrue(chooseAppPage.isListOfAppsDisplayed(), "The list of apps is not displayed.");

        AppApprovePage appApprovePage = chooseAppPage.
                clickAppByName(appName)
                .waitForPageLoad();
        Assert.assertTrue(appApprovePage.getNameOfApp().equals(appName), "The app is not opened.");
        appApprovePage.tapOnDataTimeField();
        Assert.assertTrue(appApprovePage.isCalendarWidgetOpened(), "The calendar is not opened after clicking on 'date/time' field");
        appApprovePage
                .tapOnCurrentDate()
                .tapOnOKBtn();
        Assert.assertTrue(appApprovePage.isClockWidgetOpened(), "The 'Clock' is not opened after clicking on the date in the 'Calendar' widget");

        appApprovePage
                .tapOnCurrentTime()
                .tapOnOKBtn();

        String dateAndTime = appApprovePage.getDateTimeField();
        Assert.assertTrue(appApprovePage
                .enterTitle(title)
                .waitForPageLoad()
                .getTitle()
                .equals(title), "The title is not entered");

        SubmitActivityPopup submitActivityPopup = appApprovePage.clickCheckmark();
        Assert.assertTrue(submitActivityPopup.isPopupVisible(), "The confirmation pop-up is not displayed");
        home = submitActivityPopup.clickYes().waitForPageLoad();
        Assert.assertTrue(home.isSuccessfulSubmittingMessageShown(), "The message about the successful submitting is not shown");

        ChooseViewsPage chooseViewsPage = home.clickViewButton();
        Assert.assertTrue(chooseViewsPage.isListOfActivitiesDisplayed(), "The activities are not shown in the 'View activities' page");

        Assert.assertTrue(chooseAppPage
                .clickAppByTitle(title)
                .waitForPageLoad()
                .getDateTimeField()
                .equals(dateAndTime), "The date and time are displayed incorrectly on the activity page");
    }


    @Test
    @Combine
    @Description("Make sure the activities are synced after pulling down")
    public void tc606764() {
        String appName = "testAppName1";
        String viewName = "testTitle" + RandomStringUtils.randomAlphabetic(5);
        String sampleText = "testSample" + RandomStringUtils.randomAlphabetic(5);
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        toggleWiFi(false);
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        Assert.assertTrue(chooseAppPage.isListOfAppsDisplayed(), "The list of apps is not displayed.");
        SubmitActivityPopup submitActivity = chooseAppPage
                .clickAppByName(appName)
                .waitForPageLoad()
                .enterTitle(viewName)
                .enterSampleItem(sampleText)
                .clickCheckmark();
        home = submitActivity.clickYes().waitForPageLoad();
        ChooseViewsPage views = home.clickViewButton().waitForPageLoad();
        toggleWiFi(true);
        views.swipeActivities();//works slowly, maybe need to optimization
        Assert.assertTrue(views.isListOfActivitiesDisplayed(), "The activities are not shown in the 'View activities' page");
        //---web------
        pages.web.LoginPage loginPage = page(pages.web.LoginPage.class);
        MenuPage menuPage = loginPage.enterEmail(System.getProperty("username"))
                .enterPass(System.getProperty("userpass"))
                .clickLogin();
        pages.web.ChooseViewsPage viewsPage = menuPage.clickView().waitForPageLoad();
        Assert.assertTrue(viewsPage.isViewPresent(viewName), "Created view should be exists on web");
    }

    @Test
    @Description("Make sure the activities added on web site are displayed in the application")
    @Combine
    public void tc606767() {
        String appName = "testAppName1";
        String activityName = "testActivity" + RandomStringUtils.randomAlphabetic(5);
        String sampleItem = "sample item";
        String signatoryName = "signatory";
        //----mobile-------
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        //------web--------
        pages.web.LoginPage loginPage = page(pages.web.LoginPage.class);
        AppsLibraryPage menuPage = loginPage.enterEmail(System.getProperty("username"))
                .enterPass(System.getProperty("userpass"))
                .clickLogin();
        //---mobile----------------
        ChooseViewsPage views = home
                .waitForPageLoad()
                .clickViewButton()
                .waitForPageLoad();
        //---web-----------
        ReportNewActivityPage activityPage = menuPage.waitForPageLoad().clickNew().clickAppByName(appName);
        activityPage.enterTitleField(activityName).enterSampleItemField(sampleItem)
                .clickSubmit();
//----mobile-----------
        views.swipeActivities();
        Assert.assertTrue(views.isViewPresent(activityName), "The added activity is not exists or has not status 'Sync pending'");
    }

    @Test
    @Description("Make sure the 'Add photo' app element can be filled")
    public void tc606751() throws IOException {
        String appName = "My testing app (Add a photo)";
        String title = "testTitle" + RandomStringUtils.randomAlphabetic(5);
        new LoginAction().loginActionBasic();
        HomePage home = new HomePage();
        home.waitSpinner();
        ChooseAppPage chooseAppPage = home
                .waitForPageLoad()
                .clickNewButton()
                .waitForPageLoad();
        Assert.assertTrue(chooseAppPage.isListOfAppsDisplayed(), "The list of apps is not displayed.");
        AppApprovePage appApprovePage = chooseAppPage.
                clickAppByName(appName)
                .waitForPageLoad();
        Assert.assertTrue(appApprovePage.getNameOfApp().equals(appName), "The app is not opened.");
        Assert.assertTrue(appApprovePage
                .tapOnAddImageIcon()
                .isMenuForAddingPhotoOpened(), "The 'Add a photo' menu is not opened");
        appApprovePage.tapOnAddFromLibraryButton();
        // switches the context to the native from the webview context
        appApprovePage.setContextToNative();
        appApprovePage
                .tapOnImageInAndroidGalleryApp()
                .tapOnOKButtonInAndroidGalleryApp();

        appApprovePage
                .tapOnCheckmarkPhotoBtn();
        // switches the context to the webview from the native context
        driver.context(Constants.SEEFORGE_WEB_CONTEXT);
        Assert.assertTrue(appApprovePage
                .waitForPageLoad()
                .isPhotoLoupeDisplayed(), "The photo is not saved or the photo thumb not is displayed on the app page");
        appApprovePage.tapOnPhotoLoupe();
        BufferedImage firstImage = appApprovePage.getAddedImageScreenshot();
        appApprovePage.tapOnCloseBtnInPhotoModal();
        Assert.assertTrue(appApprovePage
                .enterTitle(title)
                .waitForPageLoad()
                .getTitle()
                .equals(title), "The title is not entered");
        SubmitActivityPopup submitActivityPopup = appApprovePage.clickCheckmark();
        Assert.assertTrue(submitActivityPopup.isPopupVisible(), "The confirmation pop-up is not displayed");
        home = submitActivityPopup.clickYes().waitForPageLoad();
        Assert.assertTrue(home.isSuccessfulSubmittingMessageShown(), "The message about the successful submitting is not shown");
        ChooseViewsPage chooseViewsPage = home.clickViewButton();
        Assert.assertTrue(chooseViewsPage.isListOfActivitiesDisplayed(), "The activities are not shown in the 'View activities' page");
        appApprovePage = chooseAppPage.clickAppByTitle(title);
        BufferedImage secondImage = appApprovePage
                .waitForPageLoad()
                .tapOnPhotoLoupe()
                .waitForPageLoad()
                .getAddedImageScreenshot();
        Assert.assertFalse(getDiff(firstImage, secondImage).hasDiff(), "The Add photo activity is not displayed correctly on the active");
    }
}
