package reporting;

import properties.Properties;

public class Attachments {


    public static String attachPassedParameters() {
        String udid = Properties.getAppiumUdid();
        String osVersion = Properties.getAppiumApiVersion();
        String deviceName = Properties.getAppiumDeviceName();
        String platform = Properties.getPlatform();
        String emulatorTimeout = Properties.getAppiumEmulatorTimeout();
        String emulator = Properties.getEmulator();
        String devicePassword = Properties.getDevicePassword();
        String npmPackagesFolderPath = Properties.getNpmPackagesFolderPath();
        return "platform: " + platform + "\n" +
                "os version: " + osVersion + "\n" +
                "udid: " + udid + "\n" +
                "device name: " + deviceName + "\n" +
                "emulator timeout: " + emulatorTimeout + "\n" +
                "emulator: " + emulator + "\n" +
                "device password: " + devicePassword + "\n" +
                "npm packages folder path: " + npmPackagesFolderPath;
    }
}
