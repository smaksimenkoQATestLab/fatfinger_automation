package utils;

import java.io.File;

public class Constants {
    public final static int ELEMENT_120_TIMEOUT_SECONDS = 120;
    public final static int ELEMENT_30_TIMEOUT_SECONDS = 30;
    public final static int ELEMENT_10_TIMEOUT_SECONDS = 10;
    public final static int ELEMENT_1_TIMEOUT_SECONDS = 1;

    public final static String sdkPath = System.getenv("ANDROID_HOME");
    public final static String adbPath = sdkPath + File.separator + "platform-tools" + File.separator + "adb";

    public final static String APP_NAME = "config/android-debug.apk";

    public final static String yearDateForCalendarPattern = "yyyy";
    public final static String dayDateForCalendarPattern = "EEE, d MMM";
    public final static String dateForClickingInCalendarPattern = "dd MMMM YYYY";
    public final static String hourForClickingInCalendarPattern = "h";
    public final static String minuteForClickingInCalendarPattern = "m";
    public final static String SEEFORGE_WEB_CONTEXT = "WEBVIEW_com.seeforge.crossplatform.test";
}
