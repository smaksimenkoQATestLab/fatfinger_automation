package actions;

import base.BaseTest;
import org.testng.Assert;
import pages.HomePage;
import pages.LoginPage;
import pages.WelcomeSlidesPage;

public class LoginAction extends BaseTest {
    LoginPage loginPage;
    WelcomeSlidesPage welcomePage;

    public LoginAction loginValidate(){
        loginAction(System.getProperty("username"),System.getProperty("userpass"));
        return this;
    }

    public void passWelcomeSlide() {
        welcomePage = new WelcomeSlidesPage();
        welcomePage.clickSkip();
    }

    public LoginAction loginAction(String username, String userpass) {
        loginPage = new LoginPage();
        loginPage.isPageOpened();
        loginPage.enterEmail(username)
                .enterPass(userpass)
                .clickLogin();
        welcomePage = new WelcomeSlidesPage();
        Assert.assertTrue(welcomePage.isWelcomeScreenVisible(), "Welcome screen doesn't appear");
        return this;
    }

    public HomePage loginActionBasic() {
        loginAction(System.getProperty("username"),System.getProperty("userpass")).passWelcomeSlide();
        return new HomePage();
    }


}
