package pages;

import base.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import utils.Constants;

public class LoginPage extends BasePage {

    private String emailLocator = "//input[@name='email']";

    //@FindBy(accessibility = "Email address")
    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailString;

    //@FindBy(xpath = "//android.webkit.WebView[@content-desc=\"FAT FINGER\"]/android.view.View/android.view.View[1]/android.view.View[2]/android.widget.EditText")
    @FindBy(xpath = "//input[@name='password']")
    private WebElement pass;

    //@AndroidFindBy(xpath = "//android.widget.CheckBox[@content-desc=\"Remember me\"]")
    @FindBy(xpath = "//div[@class='checkbox-inner']")
    private WebElement checkboxRememberMe;

    //@FindBy(accessibility = "LOGIN ")
    @FindBy(xpath = "//button[contains(@class,'button-large')]")
    private WebElement btnLogin;

    //@AndroidFindBy(accessibility = "FORGOT YOUR PASSWORD? ")
    @FindBy(xpath = "//span[@class='button-inner' and contains(text(),'Forgot')]")
    private WebElement forgotPasswordLink;

    //@AndroidFindBy(accessibility = "SIGN UP FOR A FAT FINGER ACCOUNT ")
    @FindBy(xpath = "//span[@class='button-inner' and contains(text(),'Sign')]")
    private WebElement signUPLink;

    public LoginPage() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Step
    public LoginPage waitForPageLoad(){
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, forgotPasswordLink)
                ,this.getClass().getName() + " is not opened");
        return this;
    }

    @Step
    public boolean isPageOpened() {
        return isElementExists(Constants.ELEMENT_30_TIMEOUT_SECONDS, By.xpath(emailLocator));
    }

    @Step
    public LoginPage enterEmail(String email) {
        wait.until(ExpectedConditions.visibilityOf(emailString)).sendKeys(email);
        return this;
    }

    @Step
    public LoginPage enterPass(String password) {
        wait.until(ExpectedConditions.visibilityOf(pass)).sendKeys(password);
        return this;
    }

    @Step
    public LoginPage setCheckbox(boolean flag) {
        setCheckBox(checkboxRememberMe,flag);
        return this;
    }

    @Step
    public WelcomeSlidesPage clickLogin() {
        click(btnLogin);
        return new WelcomeSlidesPage();
    }

    @Step
    public ForgotPasswordPage clickForgotPasswordLink() {
        click(forgotPasswordLink);
        return new ForgotPasswordPage();
    }

    @Step
    public RegistrationPage clickSignUPLink() {
        click(signUPLink);
        return new RegistrationPage();
    }
}
