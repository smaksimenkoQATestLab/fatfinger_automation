package pages;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.testng.Assert;

import java.util.List;

public class ChooseTasksPage extends BasePage {

    public ChooseTasksPage() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @FindBy(xpath = "//page-tasks//button[contains(@class,'item-block')]")
    private List<WebElement> tasksList;

    @Step
    public List<WebElement> getTasksList() {
        return tasksList;
    }

    @Step
    public ChooseTasksPage waitForPageLoad() {
        waitForWebViewLoad();
        System.out.println(tasksList.size());
        Assert.assertTrue(isTasksNotEmpty()
                ,this.getClass().getName() + " is not opened");
        sleep(1000);
        return this;
    }

    @Step
    public boolean isTasksNotEmpty() {
        wait.until((ExpectedCondition< Boolean >) wdriver -> !tasksList.isEmpty());
        return tasksList.size() > 0;
    }
}
