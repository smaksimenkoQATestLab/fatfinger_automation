package pages;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.appNew.AppApprovePage;
import pages.appNew.ChooseAppPage;
import pages.appNew.CreateTaskPage;
import utils.Constants;

public class ActivityMenuPage extends BasePage {
    public ActivityMenuPage() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Step
    public ActivityMenuPage waitForPageLoad(){
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_10_TIMEOUT_SECONDS, menuContainer)
                ,this.getClass().getName() + " is not opened");
        return this;
    }

    @FindBy(xpath = "//div[@class='action-sheet-group']")
    private WebElement menuContainer;

    @FindBy(xpath = "//span[contains(.,'Edit Activity')]/ancestor::button")
    private WebElement editActivity;

    @FindBy(xpath = "//span[contains(.,'Create Sub')]/ancestor::button")
    private WebElement createSubActivity;

    @FindBy(xpath = "//span[contains(.,'Create Task')]/ancestor::button")
    private WebElement createTask;

    @Step
    public AppApprovePage clickEditActivity() {
        clickByJS(editActivity);
        return new AppApprovePage();
    }
    @Step
    public ChooseAppPage clickCreateSubActivity() {
        clickByJS(createSubActivity);
        return new ChooseAppPage();
    }

    @Step
    public CreateTaskPage clickCreateTask() {
        clickByJS(createTask);
        return new CreateTaskPage();
    }
}
