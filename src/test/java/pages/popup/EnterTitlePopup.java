package pages.popup;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Constants;

public class EnterTitlePopup extends BasePage {
    public EnterTitlePopup() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @FindBy(xpath = "//h2[contains(text(),'enter a title')]")
    private WebElement title;

    @FindBy(xpath = "//span[text()='Ok']")
    private WebElement btnOk;


    @Step
    public boolean isPopupVisible() {
        return isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, title);
    }

    @Step
    public void clickOk() {
        clickByJS(btnOk);
    }
}
