package pages.popup;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.HomePage;
import pages.appNew.AppApprovePage;
import utils.Constants;

public class SubmitActivityPopup extends BasePage {

    public SubmitActivityPopup() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    private String yesLocator = "//span[contains(text(),'Yes')]/ancestor::button[@ion-button='alert-button']";
    @FindBy(xpath = "//span[contains(text(),'Yes')]/ancestor::button[@ion-button='alert-button']")
    private WebElement btnYes;

    @FindBy(xpath = "//span[contains(text(),'Not yet')]/ancestor::button")
    private WebElement btnNotYet;

    @FindBy(xpath = "//div[contains(text(),'want to submit')]")
    private WebElement title;

    @Step
    public boolean isPopupVisible() {
        return isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, title);
    }

    @Step
    public HomePage clickYes() {
        sleep(1000);
        clickByJS(yesLocator);
        return new HomePage();
    }

    @Step
    public AppApprovePage clickNotYet() {
        clickByJS(btnNotYet);
        return new AppApprovePage();
    }
}
