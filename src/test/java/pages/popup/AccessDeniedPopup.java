package pages.popup;

import base.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Constants;

public class AccessDeniedPopup extends BasePage {

    public AccessDeniedPopup() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    private String okLocator = "//button[@ion-button='alert-button']";

    //@AndroidFindBy(accessibility = "Access Denied")
    @FindBy(xpath = "//div[@class='alert-wrapper']")
    private WebElement accessDenied;

    //@AndroidFindBy(accessibility = "OK ")
    @FindBy(xpath = "//button[@ion-button='alert-button']")
    private WebElement btnOk;

    @Step
    public boolean isPopupVisible() {
        return isElementExists(Constants.ELEMENT_30_TIMEOUT_SECONDS, By.xpath(okLocator));
    }

    @Step
    public void clickOk() {
        clickByJS(okLocator);
    }
}
