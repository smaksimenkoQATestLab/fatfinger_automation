package pages.popup;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.HomePage;
import pages.appNew.AppApprovePage;
import utils.Constants;

public class DiscardChangesPopup extends BasePage {

    public DiscardChangesPopup() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    private String yesLocator = "//span[contains(text(),'Yes')]/ancestor::button";
    @FindBy(xpath = "//span[contains(text(),'Yes')]/ancestor::button")
    private WebElement btnYes;

    @FindBy(xpath = "//span[contains(text(),'Not yet')]/ancestor::button")
    private WebElement btnNotYet;

    @FindBy(xpath = "//h2[contains(text(),'Cancel and go back')]")
    private WebElement title;

    @Step
    public DiscardChangesPopup waitForPopUpLoad() {
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, title),"Popup doesn't appear");
        return this;
    }

    @Step
    public HomePage clickYes() {
        sleep(1000);
        clickByJS(yesLocator);
        return new HomePage();
    }

    @Step
    public AppApprovePage clickNotYet() {
        clickByJS(btnNotYet);
        return new AppApprovePage();
    }
}
