package pages.popup;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.LoginPage;
import utils.Constants;

public class LogoutConfirmPopup extends BasePage {

    public LogoutConfirmPopup() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    private String yesLocator = "//span[contains(text(),'Yes')]/ancestor::button";
    @FindBy(xpath = "//span[contains(text(),'Yes')]/ancestor::button")
    private WebElement btnYes;

    @FindBy(xpath = "//span[contains(text(),'Not yet')]/ancestor::button")
    private WebElement btnNotYet;

    @FindBy(xpath = "//h2[contains(text(),'want to logout')]")
    private WebElement title;

    public boolean isPopupVisible() {
        return isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, title);
    }

    @Step
    public LoginPage clickYes() {
        //TODO have to make wait while menu completely appear
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickByJS(yesLocator);
        return new LoginPage();
    }

    @Step
    public void clickNotYet() {
        clickByJS(btnNotYet);
    }
}
