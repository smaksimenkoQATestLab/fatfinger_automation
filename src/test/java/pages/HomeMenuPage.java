package pages;

import base.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import pages.popup.LogoutConfirmPopup;
import utils.Constants;

public class HomeMenuPage extends BasePage {

    public HomeMenuPage() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    //@AndroidFindBy(accessibility = " powerLogout")

    private String logOutLocator = "//div[contains(.,'Logout')]/ancestor::button";
    @FindBy(xpath = "//div[contains(.,'Logout')]/ancestor::button")
    private WebElement btnLogOut;

    @FindBy(xpath = "//div[contains(.,'Slides')]/ancestor::button")
    private WebElement btnSlides;

    @FindBy(xpath = "//div[contains(.,'question')]/ancestor::button")
    private WebElement btnAskQuestion;


    @Step
    public HomeMenuPage waitForPageLoad(){
        waitForWebViewLoad();
        Assert.assertTrue(isElementExists(Constants.ELEMENT_10_TIMEOUT_SECONDS, By.xpath(logOutLocator))
                ,this.getClass().getName() + " is not opened");
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_10_TIMEOUT_SECONDS, btnLogOut )
                ,this.getClass().getName() + " is not opened");
        return this;
    }

    @Step
    public LogoutConfirmPopup clickLogOut() {
        click(btnLogOut);
        return new LogoutConfirmPopup();
    }

    @Step
    public WelcomeSlidesPage clickSlides() {
        click(btnSlides);
        return new WelcomeSlidesPage();
    }


}
