package pages.appNew;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import pages.popup.SubmitActivityPopup;
import utils.Constants;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class SignaturePage extends BasePage {

    private BufferedImage signScreenshot;

    public SignaturePage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    private Actions actions = new Actions(driver());

    private String canvasLocator = "//signature-pad[@id='signatureCanvas']/canvas";
    @FindBy(xpath = "//signature-pad[@id='signatureCanvas']/canvas")
    private WebElement canvas;

    @FindBy(xpath = "//input[@name='signatory']")
    private WebElement signatoryName;

    @FindBy(xpath = "//ion-modal//ion-icon[@name='checkmark']/ancestor::button")
    private WebElement checkButton;

    @Step
    public SignaturePage enterSignatoryName(String name) {
        wait.until(ExpectedConditions.visibilityOf(signatoryName)).sendKeys(name);
        return this;
    }

    @Step
    public SignaturePage waitForPageLoad(){
        Assert.assertTrue(isElementExists(Constants.ELEMENT_10_TIMEOUT_SECONDS, By.xpath(canvasLocator))
                ,this.getClass().getName() + " is not opened");
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_10_TIMEOUT_SECONDS, canvas )
                ,this.getClass().getName() + " is not opened");
        return this;
    }

    @Step
    public SignaturePage makeSignatore() throws IOException {
        ((JavascriptExecutor)driver())
                .executeScript("var ctx = arguments[0].getContext(\"2d\");ctx.moveTo(0, 0);ctx.lineTo(arguments[1],arguments[2]);ctx.stroke();"
                        ,canvas,canvas.getRect().getWidth(),canvas.getRect().getHeight());
        //BufferedImage img = getScreenshot();
        //setSignScreenshot();
        return this;
    }

    @Step
    public SubmitActivityPopup clickCheck() {
        clickByJS(checkButton);
        return new SubmitActivityPopup();
    }

    private void setSignScreenshot() throws IOException {
        signScreenshot = getElementScreenshot(canvas);
    }

    public BufferedImage getSignScreenshot() {
        return signScreenshot;
    }
}
