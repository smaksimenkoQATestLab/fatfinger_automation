package pages.appNew;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import pages.ActivityMenuPage;
import pages.popup.DiscardChangesPopup;
import pages.popup.SubmitActivityPopup;
import utils.Constants;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AppApprovePage extends BasePage {

    private String titleNameLocator = "(//ion-header//div[contains(@class,'toolbar-title')])[last()]";
    @FindBy(xpath = "(//ion-header//div[contains(@class,'toolbar-title')])[last()]")
    private WebElement titleName;

    @FindBy(xpath = "//input[contains(@class,'text-input')]")
    private WebElement titleField;

    @FindBy(xpath = "//ion-label[contains(.,'Enter Title')]/ancestor::button")
    private WebElement btnTitleField;

    @FindBy(xpath = "//ion-label[contains(.,'Sample Item')]/ancestor::button")
    private WebElement btnSampleItemField;

    @FindBy(xpath = "//ion-label[text()='Sample Item']/..//input")
    private WebElement sampleItemField;

    @FindBy(xpath = "//ion-modal//ion-icon[@name='checkmark']/ancestor::button")
    private WebElement checkButtonOnInputPage;

    @FindBy(xpath = "//ion-icon[@name='checkmark']/ancestor::button")
    private WebElement checkButton;

    @FindBy(xpath = "//ion-icon[@name='close']/ancestor::button")
    private WebElement closeButton;

    @FindBy(xpath = "//ion-icon[@name='more']")
    private WebElement btnMenu;

    @FindBy(xpath = "//ion-label[contains(.,'Signature')]/ancestor::button")
    private WebElement btnSignature;

    @FindBy(xpath = "//ion-col[@class='col']/img")
    private WebElement signature;

    @FindBy(xpath = "//ion-label[text()='Signature']/following-sibling::ion-note")
    private WebElement signatureName;

    @FindBy(xpath = "//ion-label[contains(.,'1 answer')]/ancestor::button")
    private WebElement btnOneAnswer;

    @FindBy(xpath = "//ion-item[contains(@class,'item-radio')]//button")
    private List<WebElement> radioItems;

    @FindBy(xpath = "//ion-segment-button[contains(., 'Yes')]")
    private WebElement yesButtonInTheTrueFalseField;

    @FindBy(xpath = "//ion-segment-button[contains(., 'No')]")
    private WebElement noButtonInTheTrueFalseField;

    @FindBy(xpath = "//button[contains(., 'date/time')]")
    private WebElement btnDataTime;


    @FindBy(xpath = "//ion-icon[@name = 'add' and @role = 'img']")
    private WebElement addImageIcon;


    @FindBy(className = "action-sheet-container")
    private WebElement actionSheetContainer;


    @FindBy(xpath = "//button[. = 'Use Camera']")
    private WebElement useCameraBtn;


    @FindBy(xpath = "//button[. = 'Add from Library']")
    private WebElement addFromLibraryBtn;


    @FindBy(xpath = "//div[@class='input-wrapper' and contains(., 'Add a Photo')]//ion-icon")
    private WebElement photoLoupe;

    @FindBy(className = "img-loaded")
    private WebElement fullScreenLoadedImage;

    @FindBy(xpath = "//photo-modal//button")
    private WebElement closePhotoModalBtn;


    /*********************************************************
     *             Elements from the native context              *
     *********************************************************/


    @FindBy(className = "android.widget.FrameLayout")
    private WebElement widget;


    @FindBy(id = "android:id/month_view")
    private WebElement monthViewElement;


    @FindBy(id = "android:id/time_header")
    private WebElement timeHeaderLabel;


    @FindBy(id = "android:id/radial_picker")
    private WebElement radialPicker;


    @FindBy(id = "android:id/date_picker_header_year")
    private WebElement yearElementInCalendarWidget;


    @FindBy(id = "android:id/date_picker_header_date")
    private WebElement dateElementInCalendarWidget;


    @FindBy(xpath = "//*[@class = 'android.widget.Button' and @text = 'OK']")
    private WebElement btnOK;


    @FindBy(xpath = "//*[@class = 'android.widget.Button' and @text = 'CANCEL']")
    private WebElement btnCANCEL;


    @FindBy(id = "com.seeforge.crossplatform.test:id/actionbar_done_textview")
    private WebElement btnOKInGalleryDialog;


    @FindBy(id = "com.seeforge.crossplatform.test:id/actionbar_discard_textview")
    private WebElement btnCANCELInGalleryDialog;


    @FindBy(className = "android.widget.ImageView")
    private WebElement image;


    @FindBy(xpath = "//*[@class = 'android.widget.Button' and @text = 'checkmark ']")
    private WebElement checkmarkPhotoBtn;


    @FindBy(xpath = "//*[@class = 'android.widget.Button' and @text = 'close ']")

    private WebElement cancelPhotoBtn;

    private String toastLocator = "div.toast-message";

    public AppApprovePage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver(), Duration.ofSeconds(10)), this);
    }

    @Step
    public AppApprovePage waitForPageLoad() {
        waitForWebViewLoad();
        Assert.assertTrue(isElementExists(Constants.ELEMENT_30_TIMEOUT_SECONDS, By.xpath(titleNameLocator)));
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, titleName)
                , this.getClass().getName() + " is not opened");
        return this;
    }

    @Step
    public AppApprovePage enterTitle(String text) {
        clickByJS(btnTitleField);
        titleField.clear();
        titleField.sendKeys(text);
        clickByJS(checkButtonOnInputPage);
        return this;
    }

    @Step
    public String getTitle() {
        return btnTitleField.findElement(By.tagName("ion-note")).getText();
    }

    @Step
    public String getNameOfApp() {
        return titleName.getText();
    }

    @Step
    public AppApprovePage enterSampleItem(String text) {
        clickByJS(btnSampleItemField);
        titleField.sendKeys(text);
        clickByJS(checkButtonOnInputPage);
        return this;
    }

    @Step("Gets the sample item field text")
    public String getSampleItemAlreadyFilledText() {
        return btnSampleItemField.findElement(By.xpath(".//ion-note")).getText();
    }

    @Step
    public String getSampleItem() {
        return sampleItemField.getAttribute("ng-reflect-model");
    }

    @Step
    public SubmitActivityPopup clickCheckmark() {
        clickByJS(checkButton);
        return new SubmitActivityPopup();
    }

    @Step
    public DiscardChangesPopup clickClose() {
        clickByJS(closeButton);
        return new DiscardChangesPopup();
    }

    @Step
    public ActivityMenuPage clickMenu() {
        clickByJS(btnMenu);
        return new ActivityMenuPage();
    }

    @Step
    public SignaturePage clickSignature() {
        clickByJS(btnSignature);
        return new SignaturePage();
    }

    public BufferedImage getSignScreenshot() throws IOException {
        return getElementScreenshot(signature);
    }

    @Step
    public String getSignatureName() {
        return signatureName.getText();
    }

    @Step
    public AppApprovePage clickOneAnswer(int index) {
        clickByJS(btnOneAnswer);
        selectOneAnswer(index);
        return this;
    }

    @Step
    public void selectOneAnswer(int index) {
        sleep(1000);
        wait.until((ExpectedCondition<Boolean>) wdriver -> radioItems.size() >= (index - 1));
        radioItems.get(index).click();
    }

    @Step
    public AppApprovePage waitForToastMessage() {
        Assert.assertTrue(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(toastLocator))).isDisplayed()
                , "Toast message doesn't appear");
        return this;
    }

    @Step("Taps the 'Yes' button in the 'True/false' field")
    public AppApprovePage tapsOnYesButtonInTheTrueFalseField() {
        wait.until(ExpectedConditions.elementToBeClickable(yesButtonInTheTrueFalseField))
                .click();
        return this;
    }

    @Step("Taps the 'No' button in the 'True/false' field")
    public AppApprovePage tapsOnNoButtonInTheTrueFalseField() {
        wait.until(ExpectedConditions.elementToBeClickable(noButtonInTheTrueFalseField))
                .click();
        return this;
    }

    @Step("Checks that the 'Yes' button is green after clicking on it")
    public boolean isYesButtonGreenAfterTapingOn() {
        return wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(yesButtonInTheTrueFalseField)))
                .getAttribute("class")
                .contains("segment-activated");
    }

    @Step("Check that the 'No' button is red after clicking on it")
    public boolean isNoButtonRedAfterTapingOn() {
        return wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(noButtonInTheTrueFalseField)))
                .getAttribute("class")
                .contains("segment-activated");
    }

    @Step("Taps on the 'date/time' field")
    public AppApprovePage tapOnDataTimeField() {
        wait.until(ExpectedConditions.elementToBeClickable(btnDataTime))
                .click();
        return this;
    }

    @Step("Checks that the calendar widget is opened")
    public boolean isCalendarWidgetOpened() {
        setContextToNative();
        boolean result = wait.until(ExpectedConditions.visibilityOf(monthViewElement)).isDisplayed();
        setContextToWebview();
        return result;
    }

    @Step("Checks that the clock widget is opened")
    public boolean isClockWidgetOpened() {
        setContextToNative();
        boolean result = wait.until(ExpectedConditions.visibilityOf(timeHeaderLabel)).isDisplayed()
                && wait.until(ExpectedConditions.visibilityOf(radialPicker)).isDisplayed();
        setContextToWebview();
        return result;
    }

    @Step("Taps on the current date in the calendar widget")
    public AppApprovePage tapOnCurrentDate() {
        setContextToNative();
        SimpleDateFormat currentDateFormat = new SimpleDateFormat(Constants.dateForClickingInCalendarPattern, Locale.US);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                "//*[@class = 'android.view.View' and @content-desc = '" + currentDateFormat.format(new Date()) + "']")))
                .click();
        setContextToWebview();
        return this;
    }

    @Step("Taps on the current hour and minute in the calendar widget")
    public AppApprovePage tapOnCurrentTime() {
        setContextToNative();
        SimpleDateFormat currentHourFormat = new SimpleDateFormat(Constants.hourForClickingInCalendarPattern, Locale.US);
        SimpleDateFormat currentMinuteFormat = new SimpleDateFormat(Constants.minuteForClickingInCalendarPattern, Locale.US);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                "//*[@class = 'android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc = '" + currentHourFormat.format(new Date()) + "']")))
                .click();
        int roundMinute = Integer.parseInt(currentMinuteFormat.format(new Date())) / 5 * 5;
        wait.until(ExpectedConditions.elementToBeClickable(driver().findElementByAccessibilityId(String.valueOf(roundMinute))))
                //       "//*[@class = 'android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc = '" + currentMinuteFormat.format(new Date()) + "']")))
                .click();
        setContextToWebview();
        return this;
    }

    @Step("Clicks on the 'OK' button in the widget")
    public AppApprovePage tapOnOKBtn() {
        setContextToNative();
        wait.until(ExpectedConditions.elementToBeClickable(btnOK)).click();
        setContextToWebview();
        return this;
    }

    @Step("Clicks on the 'CANCEL' button in the widget")
    public AppApprovePage tapOnCANCELBtn() {
        setContextToNative();
        wait.until(ExpectedConditions.elementToBeClickable(btnCANCEL)).click();
        setContextToWebview();
        return this;
    }

    @Step("Gets the value of the 'date/time' button")
    public String getDateTimeField() {
        return btnDataTime.getText();
    }

    @Step("Taps on the \"Add\" image icon")
    public AppApprovePage tapOnAddImageIcon() {
        wait.until(ExpectedConditions.elementToBeClickable(addImageIcon))
                .click();
        return this;
    }


    @Step("Checks that the menu for adding a photo is opened")
    public boolean isMenuForAddingPhotoOpened() {
        return wait.until(ExpectedConditions.visibilityOf(actionSheetContainer)).isDisplayed()
                && wait.until(ExpectedConditions.visibilityOf(actionSheetContainer)).getText().contains("Add a Photo");
    }


    @Step("Taps on the \"Use Camera\" button")
    public AppApprovePage tapOnUseCameraButton() {
        wait.until(ExpectedConditions.elementToBeClickable(useCameraBtn))
                .click();
        return this;
    }


    @Step("Taps on the \"Add from Library\" button")
    public AppApprovePage tapOnAddFromLibraryButton() {
        wait.until(ExpectedConditions.elementToBeClickable(addFromLibraryBtn))
                .click();
        return this;

    }


    @Step("Taps on an image in the android gallery app")

    public AppApprovePage tapOnImageInAndroidGalleryApp() {
        wait.until(ExpectedConditions.elementToBeClickable(image))
                .click();
        return this;
    }


    @Step("Taps on the \"OK\" button in the android gallery app")
    public AppApprovePage tapOnOKButtonInAndroidGalleryApp() {
        wait.until(ExpectedConditions.elementToBeClickable(btnOKInGalleryDialog))
                .click();
        return this;
    }


    @Step("Taps on the \"Checkmark\" button in the photo approve dialog")
    public AppApprovePage tapOnCheckmarkPhotoBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(checkmarkPhotoBtn))
                .click();
        return this;
    }


    @Step("Checks that the photo loupe is displayed")
    public boolean isPhotoLoupeDisplayed() {
        return wait.until(ExpectedConditions.visibilityOf(photoLoupe))
                .isDisplayed();
    }


    @Step("Taps on the photo loupe")

    public AppApprovePage tapOnPhotoLoupe() {
        wait.until(ExpectedConditions.elementToBeClickable(photoLoupe))
                .click();
        return this;
    }

    @Step("Gets the image screenshot of the added photo")
    public BufferedImage getAddedImageScreenshot() throws IOException {
        return getElementScreenshot(wait.until(ExpectedConditions.visibilityOf(fullScreenLoadedImage)));

    }

    @Step("Taps on the \"Close\" button in the photo modal dialog")
    public AppApprovePage tapOnCloseBtnInPhotoModal() {
        wait.until(ExpectedConditions.elementToBeClickable(closePhotoModalBtn))
                .click();
        return this;
    }
}
