package pages.appNew;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.List;

public class CreateTaskPage extends BasePage {

    public CreateTaskPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @FindBy(xpath = "//ion-label[text()='Assign To']/ancestor::button")
    private WebElement btnassignTo;

    @FindBy(xpath = "//button[@ion-button='item-cover']")
    private List<WebElement> listUsers;

    @FindBy(xpath = "//ion-icon[@name='checkmark']/ancestor::button")
    private WebElement checkButton;


    public CreateTaskPage waitForUsersListLoad() {
        wait.until((ExpectedCondition< Boolean >) wdriver -> !listUsers.isEmpty());
        return this;
    }

    @Step
    public CreateTaskPage assignTo(int index) {
        clickByJS(btnassignTo);
        waitForUsersListLoad();
        listUsers.get(index).click();
        return this;
    }

    @Step
    public AppApprovePage clickCheckmark() {
        clickByJS(checkButton);
        return new AppApprovePage();
    }
}
