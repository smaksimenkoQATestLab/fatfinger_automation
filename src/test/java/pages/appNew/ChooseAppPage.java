package pages.appNew;

import base.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import utils.Constants;

import java.util.List;

public class ChooseAppPage extends BasePage {

    //@AndroidFindBy(accessibility = "Choose App")
    @FindBy(xpath = "//div[text()='Choose App']")
    private WebElement headerText;

    //@FindBy(xpath = "//ion-content//h2")
    @FindBy(xpath = "//button[contains(@class,'item-block')]")
    private List<WebElement> appsList;


    public ChooseAppPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Step
    public ChooseAppPage waitForPageLoad(){
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, headerText)
                ,this.getClass().getName() + " is not opened");
        return this;
    }

    //for Native
//    public boolean isAppPresent(String appName) {
//        return appsList.stream()
//                .anyMatch(mobileElement -> mobileElement.getAttribute("name").equals("create" + appName + " "));
//    }

    @Step
    public boolean isAppPresent(String appName) {
        boolean isPresent = appsList.stream()
                .anyMatch(mobileElement -> mobileElement.getText().equals(appName));
        return isPresent;
    }

    @Step
    public AppApprovePage clickAppByName(String appName) {
        appsList.stream()
                .filter(mobileElement -> mobileElement.getText().equals(appName))
                .findFirst()
                .get()
                .click();
        return new AppApprovePage();
    }

    @Step
    public AppApprovePage clickAppByTitle(String title) {
        appsList.stream()
                .filter(mobileElement -> mobileElement.getText().contains(title))
                .findFirst()
                .get()
                .click();
        return new AppApprovePage();
    }

    @Step("Checks that the list of apps is present on the \"App Approve Page\"")
    public boolean isListOfAppsDisplayed() {
        // skips the first four button that are placed on the left menu and actually which are not apps
        return  wait.until(ExpectedConditions.visibilityOfAllElements(appsList.subList(4, appsList.size() - 1)))
                .stream()
                .allMatch(app -> app.isDisplayed());
    }
}
