package pages.web;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class ReportNewActivityPage {
    @FindBy(css = "input[name=reportTitle]")
    private SelenideElement titleField;

    @FindBy(css = "input[placeholder='Enter Value']")
    private SelenideElement sampleItemField;

    @FindBy(css = "input[placeholder='Signatory']")
    private SelenideElement signatureNameField;

    @FindBy(css = "button.ladda-button")
    private SelenideElement btnSubmit;

    @FindBy(css = "canvas.signPad")
    private SelenideElement signature;

    @FindBy(css = "div.locationGroup  input")
    private SelenideElement locationField;

    @Step
    public ReportNewActivityPage enterTitleField(String text) {
        titleField.shouldBe(visible).val(text);
        return this;
    }

    @Step
    public ReportNewActivityPage enterSampleItemField(String text) {
        sampleItemField.shouldBe(visible).val(text);
        return this;
    }

    @Step
    public ReportNewActivityPage enterSignatoryNameField(String text) {
        signatureNameField.shouldBe(visible).val(text);
        return this;
    }

    @Step
    public ReportNewActivityPage clickSubmit() {
        btnSubmit.shouldBe(visible).click();
        return this;
    }

    @Step
    public ReportNewActivityPage makeSignature() {
        signature.shouldBe(visible);
        ((JavascriptExecutor)getWebDriver())
                .executeScript("var ctx = arguments[0].getContext(\"2d\");ctx.moveTo(0, 0);ctx.lineTo(arguments[1],arguments[2]);ctx.stroke();"
                        ,signature,signature.getRect().getWidth(),signature.getRect().getHeight());
        return this;
    }
}
