package pages.web;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Selenide.page;

public class ChooseAppViewPage {
    @FindBy(css = "ul[role=menu] ul li.ng-scope button")
    private ElementsCollection appsNames;

    @Step
    public ReportNewActivityPage clickAppByName(String name) {
        appsNames.shouldBe(sizeGreaterThan(0));
        appsNames.stream().filter(o -> o.getText().trim().equals(name)).findFirst().get().click();
        return page(ReportNewActivityPage.class);
    }

}
