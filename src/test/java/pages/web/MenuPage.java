package pages.web;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;

public class MenuPage extends BasePage{

    @FindBy(css = "#tasksTab button")
    private SelenideElement btnTasts;

    @FindBy(xpath = "//span[contains(.,'Start')]/ancestor::button")
    private SelenideElement btnCreateNewApp;

    @FindBy(css = "#mainMenu > button")
    private SelenideElement btnBurgerMenu;

    @FindBy(css = "div.modal-body")
    private SelenideElement tutorialWindow;

    @FindBy(css = "div.navbar-collapse button[data-toggle=dropdown]")
    private SelenideElement btnNew;

    @FindBy(css = "#activityTab button")
    private SelenideElement btnView;


    @Step
    public BurgerMenuPage clickBurgerMenu() {
        btnBurgerMenu.shouldBe(visible).click();
        return page(BurgerMenuPage.class);
    }

    @Step
    public ChooseAppViewPage clickNew() {
        btnNew.shouldBe(visible).click();
        return page(ChooseAppViewPage.class);
    }

    @Step
    public ChooseViewsPage clickView() {
        btnView.shouldBe(visible).click();
        return page(ChooseViewsPage.class);
    }

    @Step
    public ChooseTasksPage clickTasks() {
        btnTasts.shouldBe(visible).click();
        return page(ChooseTasksPage.class);
    }
}
