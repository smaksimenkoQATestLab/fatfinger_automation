package pages.web;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;

public class ChooseViewsPage {
    @FindBy(css = "table[ng-table='tableParams'] tr[ng-class]")
    private ElementsCollection views;

    public ChooseViewsPage waitForPageLoad() {
        views.shouldBe(sizeGreaterThan(0));
        return this;
    }

    @Step
    public void clickAppByName(String name) {
        views.stream().filter(o -> o.$("td[data-title=\"'Title'\"]").getText().equals(name)).findFirst().get().click();
    }

    @Step
    public boolean isViewPresent(String name) {
        return views.stream().filter(o -> o.$("td[data-title=\"'Title'\"]").getText().equals(name)).findFirst().get().exists();
    }
}
