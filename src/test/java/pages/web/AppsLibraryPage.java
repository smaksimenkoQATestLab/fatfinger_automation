package pages.web;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;

public class AppsLibraryPage extends MenuPage{
    @FindBy(css = "tr[ng-repeat='form in $data']")
    private ElementsCollection appItems;

    @FindBy(css = "button[ng-click='confirm()']")
    private SelenideElement btnYes;

    @FindBy(css = "input#SearchFilter")
    private SelenideElement searchField;

    @Step
    public AppsLibraryPage switchStatus(String name) {
        getAppByName(name).$("td[data-title=\"'Status'\"] div.switchLabel").shouldBe(visible).click();
        return this;
    }

    private SelenideElement getAppByName(String name) {
        appItems.shouldHave(sizeGreaterThan(0));
        return appItems.stream().filter(o -> o.$("td > span").getText().trim().equals(name)).findFirst().get();
    }

    @Step
    public AppsLibraryPage clickYes() {
        btnYes.shouldBe(visible).click();
        return this;
    }

    @Step
    public AppsLibraryPage checkStatus(String name, Status status) {
        getAppByName(name).$("td[data-title=\"'Status'\"] span").shouldHave(text(status.getStatus()));
        return this;
    }

    @Step
    public AppsLibraryPage enterSearchField(String name) {
        searchField.shouldBe(visible).val(name);
        return page(AppsLibraryPage.class);
    }

    @Step
    public CreateAppPage chooseAppByName(String name) {
        getAppByName(name).click();
        return page(CreateAppPage.class);
    }

   public enum Status {
        DRAFT("Draft"),
        PUBLISHED("Published");

       public String getStatus() {
           return status;
       }

       String status;
        Status(String status) {
            this.status = status;
        }
        }

    @Step
    public AppsLibraryPage waitForPageLoad() {
        appItems.shouldBe(sizeGreaterThan(0));
        return page(AppsLibraryPage.class);
    }
}
