package pages.web;

import base.BasePage;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.support.FindBy;

import java.applet.Applet;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;

public class CreateAppPage {
    @FindBy(css = "input[name='tTemplate']")
    private SelenideElement appNameField;

    @FindBy(css = "div[ng-switch-when='SingleLineText'] input.form-control[type='text']")
    private ElementsCollection singleLineTextField;

    @FindBy(css = "#saveAndQuitBtn")
    private SelenideElement btnSaveGoBack;

    @FindBy(xpath = "//span[@class='elementTrilean']/ancestor::button")
    private SelenideElement btnYesNoNA;

    @FindBy(css = "div[ng-switch-when='Trilean'] > input")
    private ElementsCollection yesNoNAField;

    @FindBy(css = "input.invalidItemLabel")
    private ElementsCollection requiredFields;

    @FindBy(xpath = "//i[contains(@class,'fa-trash')]/ancestor::a")
    private ElementsCollection btnDelete;

    @Step
    public CreateAppPage enterAppName(String appName) {
        appNameField.shouldBe(visible).val(appName);
        return this;
    }

    @Step
    public CreateAppPage enterSingleTextField(int index, String text) {
        singleLineTextField.get(index).shouldBe(visible).val(text);
        return this;
    }

    @Step
    public AppsLibraryPage clickSaveGoBack() {
        btnSaveGoBack.shouldBe(visible).click();
        return page(AppsLibraryPage.class);
    }

    @Step
    public CreateAppPage addYesNoNAElement(String name) {
        btnYesNoNA.shouldBe(visible).click();
        yesNoNAField.shouldBe(sizeGreaterThan(0)).last().val(name);
        return this;
    }

    @Step
    public CreateAppPage fillAllReauiredFills() {
        requiredFields.forEach(o -> o.val(RandomStringUtils.randomAlphabetic(5)));
        return this;
    }

    @Step
    public CreateAppPage makeChanges() {
        if (yesNoNAField.shouldBe(sizeGreaterThan(0)).size() > 3) {
            btnDelete.last().click();
        } else addYesNoNAElement(RandomStringUtils.randomAlphabetic(5));
        return this;
    }

}
