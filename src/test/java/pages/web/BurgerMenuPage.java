package pages.web;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;

public class BurgerMenuPage {
    @FindBy(css = ".buildNavItem > button")
    private SelenideElement btnCreateNewApp;

    @FindBy(css = ".templateNavItem > button")
    private SelenideElement btnApps;

    @Step
    public CreateAppPage clickCreateNewApp() {
        btnCreateNewApp.shouldBe(visible).click();
        return page(CreateAppPage.class);
    }

    @Step
    public AppsLibraryPage openApps() {
        btnApps.shouldBe(visible).click();
        return page(AppsLibraryPage.class);
    }
}
