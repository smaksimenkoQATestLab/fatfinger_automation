package pages.web;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;

public class LoginPage  {
    @FindBy(css = "#Email")
    private SelenideElement emailField;

    @FindBy(css = "#Password")
    private SelenideElement passField;

    @FindBy(css = "input.btn-info")
    private SelenideElement btnLogin;

    @Step
    public LoginPage enterEmail(String email) {
        emailField.shouldBe(visible).val(email);
        return this;
    }

    @Step
    public LoginPage enterPass(String pass) {
        passField.shouldBe(visible).val(pass);
        return this;
    }

    @Step
    public AppsLibraryPage clickLogin() {
        btnLogin.shouldBe(visible).click();
        return page(AppsLibraryPage.class);
    }


}
