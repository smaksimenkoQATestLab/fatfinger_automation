package pages.web;

import org.openqa.selenium.JavascriptExecutor;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class BasePage {

    public void waitForNetwork(int timeoutInSeconds) {
        //Checking active ajax calls
        if (getWebDriver() instanceof JavascriptExecutor) {
            JavascriptExecutor jsDriver = (JavascriptExecutor) getWebDriver();
            for (int i = 0; i < timeoutInSeconds; i++) {
                Object numberOfAjaxConnections = jsDriver.executeScript("try {return jQuery.active;} catch(e) {return 0;}");
                if (numberOfAjaxConnections instanceof Long) {
                    Long n = (Long) numberOfAjaxConnections;
                    if (n == 0L)
                        break;
                }
                sleep(1000);
            }
        } else {
            System.out.println("Web getDriver: " + getWebDriver() + " cannot execute javascript");
        }
    }
}
