package pages.web;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;

public class ChooseTasksPage {
    @FindBy(css = "table[ng-table='tableParams'] tr[ng-class]")
    private ElementsCollection tasks;

    public ChooseTasksPage waitForPageLoad() {
        tasks.shouldBe(sizeGreaterThan(0));
        return this;
    }

    @Step
    public void clickTaskByName(String name) {
        tasks.stream().filter(o -> o.$("td[data-title=\"'Activity / App'\"] span.taskInfo span:first-child").getText().equals(name)).findFirst().get().click();
    }

    @Step
    public boolean isTaskPresent(String name) {
        return tasks.stream().filter(o -> o.$("td[data-title=\"'Activity / App'\"] span.taskInfo span:first-child").getText()
                .equals(name.trim())).findFirst().get().exists();
    }

}
