package pages;

import base.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import utils.Constants;

public class RegistrationPage extends BasePage {

    //browser page

    @AndroidFindBy(xpath = "(//android.view.View[contains(@text,'CREATE MY FREE ACCOUNT')])[2]")
    private MobileElement headerText;

    @AndroidFindBy(accessibility = "Email address")
    private MobileElement emailField;

    @AndroidFindBy(xpath = "//android.webkit.WebView[@content-desc=\"SEE Forge | Command Center - Create Account\"]/android.view.View[1]/android.view.View[5]/android.widget.EditText")
    private MobileElement passwordField;

    @AndroidFindBy(xpath = "//android.webkit.WebView[@content-desc=\"SEE Forge | Command Center - Create Account\"]/android.view.View[1]/android.view.View[6]/android.widget.EditText")
    private MobileElement passwordConfirmField;

    public RegistrationPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    public RegistrationPage waitForPageLoad(){
        setContextToNative();
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, headerText)
                ,this.getClass().getName() + " is not opened");
        return this;
    }

    public RegistrationPage enterEmail(String text) {
        emailField.sendKeys(text);
        return this;
    }

    public RegistrationPage enterPassword(String text) {
        passwordField.sendKeys(text);
        return this;
    }

    public RegistrationPage enterConfirmPassword(String text) {
        passwordConfirmField.sendKeys(text);
        return this;
    }

}
