package pages;

import base.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import pages.popup.DiscardChangesPopup;
import utils.Constants;

import java.util.List;

public class WelcomeSlidesPage extends BasePage {

    public WelcomeSlidesPage() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }
    @FindBy(xpath = "//button[contains(@aria-label,'Go to')]")
    private List<WebElement> btnSlides;

    private WebElement welcomeScreen;

    private String skipLocator = "(//span[contains(text(),'Skip')]/ancestor::button)[1]";
    //@AndroidFindBy(accessibility = "SKIP ")
    @FindBy(xpath = "(//span[@class='button-inner' and contains(text(),'Skip')]/ancestor::button)[1]")
    private WebElement btnSkip;

    @FindBy(xpath = "//span[contains(.,'Continue')]/ancestor::button")
    private WebElement btnContinue;

    public boolean isWelcomeScreenVisible() {
        return isElementExists(Constants.ELEMENT_30_TIMEOUT_SECONDS, By.xpath(skipLocator));
    }

    @Step
    public WelcomeSlidesPage waitForPopUpLoad() {
        Assert.assertTrue(isElementExists(Constants.ELEMENT_30_TIMEOUT_SECONDS, By.xpath(skipLocator)),"Welcome page isn't opened");
        return this;
    }

    @Step
    public void clickSkip() {
        sleep(1000);
        clickByJS(skipLocator);
    }

    @Step
    public HomePage clickContinue() {
        sleep(1000);
        clickByJS(btnContinue);
        return new HomePage();
    }

    @Step
    public WelcomeSlidesPage goToSlide(int index) {
        if (index >= 5)
            return null;
        click(btnSlides.get(index));
        waitForPopUpLoad();
        return this;
    }


}
