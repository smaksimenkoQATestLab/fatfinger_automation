package pages;

import base.BasePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import pages.appNew.AppApprovePage;
import utils.Constants;

import java.util.List;
import java.util.stream.Collectors;

public class ChooseViewsPage extends BasePage {
    public ChooseViewsPage() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    private String titleLocator = "//div[contains(text(),'View')]";

    @FindBy(xpath = "//div[text()='View']")
    private WebElement headerText;

    @FindBy(xpath = "//ion-content[contains(@class,'has-refresher')]//button[contains(@class,'item-block')]//h2/span")
    private List<WebElement> viewsList;

    @FindBy(xpath = "//ion-content[contains(@class,'has-refresher')]//button[contains(@class,'item-block')]")
    private List<WebElement> viewButtonsList;

    @FindBy(xpath = "//ion-icon[@name='search']/ancestor::button")
    private WebElement btnSearch;

    @FindBy(xpath = "//button[contains(@class,'show-back-button')]")
    private WebElement btnBack;

    @FindBy(xpath = "//ion-icon[@name='search']/ancestor::button")
    private WebElement search;

    @FindBy(xpath = "//input[@class='searchbar-input']")
    private WebElement searchField;

    @FindBy(xpath = "//button[contains(@class,'searchbar-clear-icon')]")
    private WebElement btnCloseSearch;

    @FindBy(xpath = "//div[contains(text(),'View')]")
    private WebElement titleBar;

    @FindBy(xpath = "//ion-icon[@name='arrow-forward']/ancestor::button")
    private WebElement btnArrowForward;

    @FindBy(xpath = "//div[@class='refresher-refreshing']")
    private WebElement spinner;

    @Step
    public ChooseViewsPage enterSearch(String view) {
        wait.until(ExpectedConditions.visibilityOf(searchField)).sendKeys(view);
        return this;
    }

    @Step
    public ChooseViewsPage clickSearch() {
        clickByJS(search);
        return this;
    }

    @Step
    public ChooseViewsPage waitForPageLoad() {
        waitForWebViewLoad();
        Assert.assertTrue(isElementExists(Constants.ELEMENT_10_TIMEOUT_SECONDS, By.xpath(titleLocator))
                , this.getClass().getName() + " is not opened");
        return this;
    }

    @Step
    public boolean isViewPresent(String viewName) {
        boolean isPresent = viewsList.stream()
                .anyMatch(mobileElement -> mobileElement.getText().equals(viewName));
        return isPresent;
    }

    @Step
    public AppApprovePage clickViewByName(String viewName) {
        sleep(2000);
        clickByJS(viewsList.stream()
                .filter(mobileElement -> mobileElement.getText().equals(viewName))
                .findFirst()
                .get());
        return new AppApprovePage();
    }

    @Step
    public ChooseViewsPage clickCloseSearch() {
        clickByJS(btnCloseSearch);
        return this;
    }

    public boolean getFoundActivitiesAmount(String viewName) {
        sleep(2000);
        return viewsList.stream()
                .filter(mobileElement -> mobileElement.getText().contains(viewName)).collect(Collectors.toList()).size() > 0;
    }

    public int getActivitiesAmount() {
        return viewsList.size();
    }

    public boolean isTitleBarExists() {
        return isElementExists(Constants.ELEMENT_10_TIMEOUT_SECONDS, By.xpath(titleLocator));
    }

    public boolean isSearchFieldEmpty() {
        System.out.println(searchField.getAttribute("value"));
        return searchField.getAttribute("value").isEmpty();

    }

    @Step
    public ChooseViewsPage clickArrowForward() {
        clickByJS(btnArrowForward);
        return this;
    }


    @Step("Checks that the list of activities is displayed")
    public boolean isListOfActivitiesDisplayed() {
        return wait.until(ExpectedConditions.visibilityOfAllElements(viewsList))
                .stream()
                .allMatch(activity -> activity.isDisplayed());
    }

    @Step("Checks the activity {0} is in sync pending")
    public boolean isActivitySyncPending(String activityForChecking) {
        return driver().findElement(
                By.xpath("//ion-content[contains(@class,'has-refresher')]//button[contains(@class,'item-block') and " +
                        "contains(., '" + activityForChecking + "')]"))
                .getText().contains("SYNC PENDING");
    }

    @Step
    public ChooseViewsPage swipeActivities() {
        setContextToNative();
        waitForWiFiConnected();
        swipe();
        setContextToWebview();
        wait.until(ExpectedConditions.invisibilityOf(spinner));
        return this;
    }

    @Step
    public AppApprovePage clickViewByIndex(int index) {
        clickByJS(viewsList.get(index));
        return new AppApprovePage();
    }
}
