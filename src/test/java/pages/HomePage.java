package pages;

import base.BasePage;
import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pages.appNew.ChooseAppPage;
import utils.Constants;

public class HomePage extends BasePage {

    public HomePage() {
        super();
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    private String logoLocator = "//img[@class='toplogo']";
    //@AndroidFindBy(accessibility = "FF_logo")
    @FindBy(xpath = "//img[@class='toplogo']")
    private WebElement logo;

    private String newLocator = "//p[text()='New']/ancestor::button";
    //@AndroidFindBy(accessibility = "createNEW ")
    @FindBy(xpath = "//p[text()='New']/ancestor::button")
    private WebElement btnNew;

    //@AndroidFindBy(accessibility = "paperVIEW ")
    @FindBy(xpath = "//p[text()='View']/ancestor::button")
    private WebElement btnView;

    //@AndroidFindBy(accessibility = "briefcaseTASKS ")
    @FindBy(xpath = "//p[text()='Tasks']/ancestor::button")
    private WebElement btnTasks;

    //@AndroidFindBy(accessibility = "pinMAP ")
    @FindBy(xpath = "//p[text()='Map']/ancestor::button")
    private WebElement btnMap;

    //@AndroidFindBy(accessibility = "menu ")
    @FindBy(xpath = "(//div[contains(@class,'toolbar-title')]/ancestor::div/preceding-sibling::button)[2]")
    private WebElement btnMenu;

    //@AndroidFindBy(accessibility = "Syncing data...")
    //@FindBy(xpath = "//div[contains(@class,'spinner')]")
    @FindBy(xpath = "//ion-spinner")
    private WebElement spinner;

    private String toastLocator = "div.toast-message";

    @Step
    public HomePage waitForPageLoad() {
        waitForWebViewLoad();
        Assert.assertTrue(isElementExists(Constants.ELEMENT_30_TIMEOUT_SECONDS, By.xpath(newLocator))
                , this.getClass().getName() + " is not opened");
        Assert.assertTrue(isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, btnNew)
                , this.getClass().getName() + " is not opened");
        //waitSpinner();
        return this;
    }

    @Step
    public HomePage isToastMessageVisible() {
        Assert.assertTrue(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(toastLocator)))
                .isDisplayed(), "Toast message doesn't appear");
        return this;
    }

    @Step
    public HomeMenuPage clickMenu() {
        clickByJS(btnMenu);
        return new HomeMenuPage();
    }

    //very long time to wait
//    public void waitSpinner() {
//        wait.until((ExpectedCondition<Boolean>) wdriver -> !spinner.isDisplayed());
//    }

    @Step
    public void waitSpinner() {
        wait.until((ExpectedCondition<Boolean>) w -> !isElementDisplayed(60,spinner));
    }

    @Step
    public ChooseAppPage clickNewButton() {
        btnNew.click();
        return new ChooseAppPage();
    }

    @Step
    public ChooseViewsPage clickViewButton() {
        clickByJS(btnView);
        return new ChooseViewsPage();
    }

    @Step("Checks that the successful submitting message shown")
    public boolean isSuccessfulSubmittingMessageShown() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.className("toast-message")))
                .getText()
                .equalsIgnoreCase("Activity successfully submitted");
    }

    @Step
    public ChooseTasksPage clickTasksButton() {
        btnTasks.click();
        return new ChooseTasksPage();
    }

    @Step
    public MapPage clickMapButton() {
        btnMap.click();
        return new MapPage();
    }

}
