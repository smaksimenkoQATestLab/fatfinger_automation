package pages;

import base.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utils.Constants;

public class MapPage extends BasePage {

    private String mapLocator = "//div[@class='gm-style']";
    @FindBy(xpath = "//div[@class='gm-style']")
    private WebElement mapContainer;

    @Step
    public MapPage waitForPageLoad() {
        waitForWebViewLoad();
        Assert.assertTrue(isElementExists(Constants.ELEMENT_10_TIMEOUT_SECONDS, (By.xpath(mapLocator)))
                ,this.getClass().getName() + " is not opened");
        return this;
    }
}
