package pages;

import base.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.Constants;

import java.util.Set;

public class ForgotPasswordPage extends BasePage {

    @AndroidFindBy(id = "Email")
    private MobileElement emailAddressField;

    @AndroidFindBy(className = "android.widget.Button")
    private MobileElement emailLinkButton;

    @AndroidFindBy(xpath = "//android.view.View[contains(@text,'reset')]")
    private MobileElement resetHeaderPasswordText;

    public ForgotPasswordPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    public ForgotPasswordPage enterEmail(String email) {
        setContextToNative();
        wait.until(ExpectedConditions.visibilityOf(emailAddressField)).sendKeys(email);
        return this;
    }

    public ForgotPasswordPage clickEmailLinkButton() {
        wait.until(ExpectedConditions.visibilityOf(emailLinkButton)).click();
        return this;
    }

    public boolean isResetHeaderTextDisplayed() {
        return isElementDisplayed(Constants.ELEMENT_30_TIMEOUT_SECONDS, resetHeaderPasswordText);
    }


}
